<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="SalesByEmployee" pageWidth="792" pageHeight="612" orientation="Landscape" columnWidth="732" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<reportFont name="title" isDefault="false" fontName="Lucida Sans Typewriter" size="18" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
	<reportFont name="normal" isDefault="false" fontName="Lucida Sans Typewriter" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
	<reportFont name="normal-bold" isDefault="false" fontName="Lucida Sans Typewriter" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
	<reportFont name="detail" isDefault="false" fontName="Lucida Sans Typewriter" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
	<reportFont name="detail-bold" isDefault="false" fontName="Lucida Sans Typewriter" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1250" isPdfEmbedded="false"/>
	<parameter name="HELPER" class="dtv.pos.reporting.fill.IReportHelper" isForPrompting="false"/>
	<parameter name="organizationId" class="java.lang.Long" isForPrompting="false"/>
	<parameter name="retailLocationId" class="java.lang.Long" isForPrompting="false"/>
	<parameter name="storeNbr" class="java.lang.String" isForPrompting="false"/>
	<parameter name="storeName" class="java.lang.String" isForPrompting="false"/>
	<parameter name="employeeId" class="java.lang.String">
		<parameterDescription><![CDATA[_rptEmployeeProductivityReportEmployee;Text;Simple;maxCharacters=30]]></parameterDescription>
		<defaultValueExpression><![CDATA["%"]]></defaultValueExpression>
	</parameter>
	<parameter name="dateRange" class="dtv.pos.storecalendar.IDtvDateRange">
		<parameterDescription><![CDATA[_rptSelectionDateRange;DateRange]]></parameterDescription>
		<defaultValueExpression><![CDATA[dtv.pos.storecalendar.IDtvDateRange.DEFAULT]]></defaultValueExpression>
	</parameter>
	<parameter name="startDate" class="java.util.Date" isForPrompting="false">
		<defaultValueExpression><![CDATA[$P{dateRange}.getStartDate()]]></defaultValueExpression>
	</parameter>
	<parameter name="endDate" class="java.util.Date" isForPrompting="false">
		<defaultValueExpression><![CDATA[$P{dateRange}.getEndDate()]]></defaultValueExpression>
	</parameter>
	<field name="business_date" class="java.util.Date"/>
	<field name="employee_id" class="java.lang.String"/>
	<field name="trans_seq" class="java.lang.String"/>
	<field name="net_amt" class="java.math.BigDecimal"/>
	<field name="price_change_amt" class="java.math.BigDecimal"/>
	<field name="discounts" class="java.math.BigDecimal"/>
	<field name="item_id" class="java.lang.String"/>
	<field name="quantity" class="java.math.BigDecimal"/>
	<field name="net_sales" class="java.math.BigDecimal"/>
	<field name="item_price" class="java.math.BigDecimal"/>
	<field name="item_description" class="java.lang.String"/>
	<field name="percentage_of_item" class="java.math.BigDecimal"/>
	<field name="commission_mod_seq_nbr" class="java.lang.Integer"/>
	<field name="wkstn_id" class="java.math.BigDecimal"/>
	<field name="gross_sales" class="java.math.BigDecimal"/>
	<field name="promotions" class="java.math.BigDecimal"/>
	<variable name="tran_net_sales" class="java.math.BigDecimal" resetType="Group" resetGroup="trans" calculation="Sum">
		<variableExpression><![CDATA[$F{commission_mod_seq_nbr} == 1 ?
$F{net_sales}:
new BigDecimal("0.00")]]></variableExpression>
	</variable>
	<variable name="total_sales" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{commission_mod_seq_nbr} == 1 ?
$F{net_sales}:
new BigDecimal("0.00")]]></variableExpression>
	</variable>
	<variable name="daily_net_sales" class="java.math.BigDecimal" resetType="Group" resetGroup="businessDate" calculation="Sum">
		<variableExpression><![CDATA[$F{commission_mod_seq_nbr} == 1 ?
$F{net_sales}:
new BigDecimal("0.00")]]></variableExpression>
	</variable>
	<group name="businessDate">
		<groupExpression><![CDATA[$F{business_date}]]></groupExpression>
		<groupHeader>
			<band height="17" splitType="Stretch">
				<textField pattern="" isBlankWhenNull="true">
					<reportElement key="textField" mode="Opaque" x="1" y="1" width="78" height="16" forecolor="#000000" backcolor="#FFFFFF"/>
					<box>
						<topPen lineWidth="0.0" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
						<font reportFont="detail-bold"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_rptBusinessDate")]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="true">
					<reportElement key="textField" mode="Opaque" x="81" y="1" width="73" height="16" forecolor="#000000" backcolor="#FFFFFF"/>
					<box>
						<topPen lineWidth="0.0" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
						<font reportFont="detail-bold"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.formatDate(new dtv.util.DtvDate($F{business_date}.getTime()))]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="23" splitType="Stretch">
				<textField pattern="" isBlankWhenNull="true">
					<reportElement key="textField" mode="Opaque" x="402" y="7" width="245" height="16" forecolor="#000000" backcolor="#FFFFFF"/>
					<box>
						<topPen lineWidth="0.0" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
						<font reportFont="detail-bold"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_rptTotalFor")+ " " + $P{HELPER}.formatDate(new dtv.util.DtvDate($F{business_date}.getTime())) + ":"]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="true">
					<reportElement key="textField" mode="Opaque" x="648" y="7" width="83" height="16" forecolor="#000000" backcolor="#FFFFFF"/>
					<box>
						<topPen lineWidth="0.0" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
						<font reportFont="detail-bold"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.formatMoney($V{daily_net_sales})]]></textFieldExpression>
				</textField>
				<line>
					<reportElement key="line" mode="Opaque" x="648" y="4" width="83" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
					<graphicElement>
						<pen lineWidth="0.25"/>
					</graphicElement>
				</line>
			</band>
		</groupFooter>
	</group>
	<group name="trans">
		<groupExpression><![CDATA[$F{trans_seq}]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band height="19" splitType="Stretch">
				<textField pattern="" isBlankWhenNull="true">
					<reportElement key="textField" mode="Opaque" x="649" y="2" width="83" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
					<box>
						<topPen lineWidth="0.0" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
						<font reportFont="detail-bold"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.formatMoney($V{tran_net_sales})]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="true">
					<reportElement key="textField" mode="Opaque" x="402" y="2" width="245" height="16" forecolor="#000000" backcolor="#FFFFFF"/>
					<box>
						<topPen lineWidth="0.0" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
						<font reportFont="detail-bold"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_rptTotalFor")+ " " + $F{trans_seq} + "#:"]]></textFieldExpression>
				</textField>
				<line>
					<reportElement key="line-1" mode="Opaque" x="648" y="1" width="83" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
					<graphicElement>
						<pen lineWidth="0.25"/>
					</graphicElement>
				</line>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band height="47" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="1" y="1" width="731" height="25" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font reportFont="title"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_rptSalesByEmployeeReport")]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="1" y="29" width="70" height="18" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font reportFont="normal"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_rptSelectionDateRange")]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="591" y="29" width="80" height="18" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font reportFont="normal"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_rptReportEmployeeHeading")]]></textFieldExpression>
			</textField>
			<line>
				<reportElement key="line" mode="Opaque" x="1" y="27" width="731" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
				<graphicElement>
					<pen lineWidth="0.25"/>
				</graphicElement>
			</line>
			<textField pattern="MM/dd/yyyy" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="73" y="29" width="467" height="18" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font reportFont="normal"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{dateRange}.getDescription().toString(dtv.i18n.OutputContextType.DOCUMENT)]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="672" y="29" width="60" height="18" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font reportFont="normal"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{employeeId} == "%" ? $P{HELPER}.translate("_rptStartEmployeeAll") : $P{employeeId}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="28" splitType="Stretch">
			<rectangle radius="0">
				<reportElement key="rectangle-1" mode="Opaque" x="1" y="1" width="731" height="27" forecolor="#000000" backcolor="#CCCCCC"/>
				<graphicElement>
					<pen lineWidth="0.25"/>
				</graphicElement>
			</rectangle>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Transparent" x="1" y="2" width="56" height="25" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font reportFont="detail-bold"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_rptReportTranDateHeading")]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Transparent" x="144" y="2" width="66" height="25" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font reportFont="detail-bold"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_rptReportEmployeeIdHeading")]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Transparent" x="58" y="2" width="56" height="25" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font reportFont="detail-bold"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_rptReportTransactionIdHeading")]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Transparent" x="214" y="2" width="72" height="25" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font reportFont="detail-bold"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_rptReportItemIdDescHeading")]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Transparent" x="355" y="2" width="60" height="25" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font reportFont="detail-bold"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_rptReportPriceHeading")]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Transparent" x="512" y="2" width="49" height="25" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font reportFont="detail-bold"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_criRptDailyCashSalesPromotions")]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Transparent" x="304" y="2" width="42" height="25" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font reportFont="detail-bold"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_rptQtyHeading")]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Transparent" x="580" y="2" width="54" height="25" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font reportFont="detail-bold"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_rptReportMarkDownHeading")]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Transparent" x="694" y="2" width="37" height="25" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font reportFont="detail-bold"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_rptReportNetSalesHeading")]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-4" mode="Transparent" x="115" y="2" width="32" height="25" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font reportFont="detail-bold"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_rptNoSaleRegID")]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Transparent" x="643" y="2" width="38" height="25" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font reportFont="detail-bold"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_criRptDailyCashSalesSalesSummaryGrossSales")]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Transparent" x="422" y="2" width="69" height="25" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font reportFont="detail-bold"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_criRptDailyCashSalesSalesSummaryPriceOverride")]]></textFieldExpression>
			</textField>
		</band>
	</columnHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" isPrintRepeatedValues="false" mode="Opaque" x="344" y="1" width="71" height="16" forecolor="#000000" backcolor="#FFFFFF">
					<printWhenExpression><![CDATA[true]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font reportFont="detail"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.formatMoney($F{item_price})]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" isPrintRepeatedValues="false" mode="Opaque" x="58" y="1" width="56" height="16" forecolor="#000000" backcolor="#FFFFFF">
					<printWhenExpression><![CDATA[true]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font reportFont="detail"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{trans_seq}]]></textFieldExpression>
			</textField>
			<textField pattern="0.00" isBlankWhenNull="true">
				<reportElement key="textField" isPrintRepeatedValues="false" mode="Opaque" x="491" y="1" width="69" height="16" forecolor="#000000" backcolor="#FFFFFF">
					<printWhenExpression><![CDATA[true]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font reportFont="detail"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.formatMoney($F{promotions})]]></textFieldExpression>
			</textField>
			<textField pattern="0" isBlankWhenNull="true">
				<reportElement key="textField" isPrintRepeatedValues="false" mode="Opaque" x="315" y="1" width="29" height="16" forecolor="#000000" backcolor="#FFFFFF">
					<printWhenExpression><![CDATA[true]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font reportFont="detail"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{quantity}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" isPrintRepeatedValues="false" mode="Opaque" x="1" y="1" width="56" height="16" forecolor="#000000" backcolor="#FFFFFF">
					<printWhenExpression><![CDATA[true]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font reportFont="detail"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.formatDate(new dtv.util.DtvDate($F{business_date}.getTime()))]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" isPrintRepeatedValues="false" mode="Opaque" x="157" y="1" width="53" height="16" isRemoveLineWhenBlank="true" forecolor="#000000" backcolor="#FFFFFF">
					<printWhenExpression><![CDATA[true]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font reportFont="detail"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{employee_id}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement key="textField" isPrintRepeatedValues="false" mode="Opaque" x="214" y="1" width="101" height="16" forecolor="#000000" backcolor="#FFFFFF">
					<printWhenExpression><![CDATA[true]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font reportFont="detail"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{item_id}+ " " + $P{HELPER}.translate($F{item_description})]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" isPrintRepeatedValues="false" mode="Opaque" x="561" y="1" width="72" height="16" forecolor="#000000" backcolor="#FFFFFF">
					<printWhenExpression><![CDATA[true]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font reportFont="detail"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.formatMoney($F{discounts})]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" isPrintRepeatedValues="false" mode="Opaque" x="671" y="1" width="60" height="16" forecolor="#000000" backcolor="#FFFFFF">
					<printWhenExpression><![CDATA[true]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font reportFont="detail"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.formatMoney($F{net_sales})]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-5" isPrintRepeatedValues="false" mode="Opaque" x="115" y="1" width="32" height="16" forecolor="#000000" backcolor="#FFFFFF">
					<printWhenExpression><![CDATA[true]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font reportFont="detail"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{wkstn_id}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" isPrintRepeatedValues="false" mode="Opaque" x="632" y="1" width="51" height="16" forecolor="#000000" backcolor="#FFFFFF">
					<printWhenExpression><![CDATA[true]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font reportFont="detail"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.formatMoney($F{gross_sales})]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" isPrintRepeatedValues="false" mode="Opaque" x="415" y="1" width="71" height="16" forecolor="#000000" backcolor="#FFFFFF">
					<printWhenExpression><![CDATA[true]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font reportFont="detail"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.formatMoney($F{price_change_amt})]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="21" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="1" y="4" width="200" height="16" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font reportFont="detail"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_rptReportRunDate")+" "+$P{HELPER}.formatDateTime(new dtv.util.DtvDate())]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="482" y="4" width="250" height="16" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font reportFont="detail"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{storeName}+"  "+ $P{HELPER}.translate("_rptStore") + $P{storeNbr}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="347" y="4" width="55" height="16" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font reportFont="detail"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_rptPage")+" "+$V{PAGE_NUMBER}+" "+$P{HELPER}.translate("_rptof")+" "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="403" y="4" width="30" height="16" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font reportFont="detail"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$V{PAGE_NUMBER}.toString()]]></textFieldExpression>
			</textField>
			<line>
				<reportElement key="line" mode="Opaque" x="1" y="1" width="731" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
				<graphicElement>
					<pen lineWidth="0.25"/>
				</graphicElement>
			</line>
		</band>
	</pageFooter>
	<summary>
		<band height="26" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="648" y="10" width="83" height="16" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font reportFont="detail-bold"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.formatMoney($V{total_sales})]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="402" y="10" width="245" height="16" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font reportFont="detail-bold"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_rptReportSummaryTotal")]]></textFieldExpression>
			</textField>
			<line>
				<reportElement key="line" mode="Opaque" x="648" y="8" width="83" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
				<graphicElement>
					<pen lineWidth="0.25"/>
				</graphicElement>
			</line>
		</band>
	</summary>
</jasperReport>
