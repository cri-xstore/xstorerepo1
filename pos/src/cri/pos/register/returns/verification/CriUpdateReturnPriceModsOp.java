package cri.pos.register.returns.verification;

import static dtv.util.NumberUtils.ONE;
import static dtv.xst.dao.trl.RetailPriceModifierReasonCode.DEAL;

import java.math.BigDecimal;

import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.register.ISaleReturnLineItemCmd;
import dtv.pos.register.returns.ReturnType;
import dtv.pos.register.returns.verification.UpdateReturnPriceModsOp;
import dtv.util.NumberUtils;
import dtv.xst.dao.trl.*;

public class CriUpdateReturnPriceModsOp
    extends UpdateReturnPriceModsOp {

  /**
   * 
   */
  private static final long serialVersionUID = -8807693534853737550L;

  /** {@inheritDoc} */
  @Override
  public IOpResponse handleOpExec(IXstCommand argCmd, IXstEvent argEvent) {
    IOpResponse response = super.handleOpExec(argCmd, argEvent);
    // Only process this if we have a verified return
    if (ReturnType.valueOf(((ISaleReturnLineItemCmd) argCmd).getLineItem().getReturnTypeCode()).isVerified()
        && response.getOpStatus().isComplete()) {
      ISaleReturnLineItem newLineItem = ((ISaleReturnLineItemCmd) argCmd).getLineItem();

      /* fix bug. verify return the line item and item's has discount. The discount itme's amount.
       * is not correct on the view port. */
      updateDiscountPriceModifiers(newLineItem);
    }

    return response;
  }

  /**
   * Update the new return line item discount line item price.
   * @param argNewLineItem
   */
  protected void updateDiscountPriceModifiers(ISaleReturnLineItem argNewLineItem) {
    for (IRetailPriceModifier priceModifier : argNewLineItem.getRetailPriceModifiers()) {
      if (!priceModifier.getVoid()) {
        RetailPriceModifierReasonCode modifierType =
            RetailPriceModifierReasonCode.forName(priceModifier.getRetailPriceModifierReasonCode());

        if ((modifierType != null) && modifierType.isDiscount()) {
          if (DEAL.equals(modifierType)) {
            BigDecimal returnedQty = NumberUtils.nonNull(argNewLineItem.getQuantity());

            if (NumberUtils.isGreaterThan(returnedQty, ONE)) {
              priceModifier.setAmount(priceModifier.getAmount().multiply(returnedQty));
            }
          }
        }
      }
    }
  }
}
