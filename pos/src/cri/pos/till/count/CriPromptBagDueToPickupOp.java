//$Id$
package cri.pos.till.count;

import java.util.List;

import dtv.pos.framework.ui.op.AbstractListPromptOp;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;
import dtv.pos.iframework.ui.config.IPromptKey;

/**
 * Prompt the due to pickup deposit bags.<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 *
 * @author shy xie
 * @created Jul 31, 2012
 * @version $Revision$
 */
public class CriPromptBagDueToPickupOp
    extends AbstractListPromptOp {

  /**
   * 
   */
  private static final long serialVersionUID = -432733746879909773L;

  /** {@inheritDoc} */
  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    return !getDueToPickupBagItem(argCmd).isEmpty();
  }

  @Override
  protected IOpResponse handlePromptResponse(IXstCommand argCmd, IXstEvent argEvent) {
    return HELPER.completeResponse();
  }

  @Override
  protected IPromptKey getEmptyListPromptKey(IXstCommand argCmd) {
    return null;
  }

  @Override
  protected Object[] getPromptList(IXstCommand argCmd, IXstEvent argEvent) {
    return getDueToPickupBagItem(argCmd).toArray();
  }

  protected List<CriBagSummaryItem> getDueToPickupBagItem(IXstCommand argCmd) {
    CriTillCountCmd cmd = (CriTillCountCmd) argCmd;
    return (cmd.getBagSummaryModel().getDueToPickupBagSummaryList());
  }
}
