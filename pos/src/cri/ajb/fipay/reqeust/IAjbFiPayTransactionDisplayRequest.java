//$Id$
package cri.ajb.fipay.reqeust;

import cri.ajb.state.DeviceTranState;

/**
 * AJB transaction rolling receipt request.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision$
 */
public interface IAjbFiPayTransactionDisplayRequest {

  /**
   * Get the rolling receipt transaction data.
   * 
   * @return
   */
  public DeviceTranState getTransactionState();

}
