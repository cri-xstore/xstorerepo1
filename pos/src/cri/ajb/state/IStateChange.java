//$Id$
package cri.ajb.state;

/**
 * State change.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 *
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision$
 */
public interface IStateChange {

  /**
   * Get the display text.
   *
   * @return
   */
  public String getNewText();
}
