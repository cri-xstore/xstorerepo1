package com.micros_retail.xcenter.ibm.webcommerce;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.core.Message;
import org.springframework.integration.message.MessageBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.micros_retail.util.StringUtils;
import com.micros_retail.util.logging.Logger;
import com.micros_retail.xcenter.bootstrap.XcenterProperties;
import com.micros_retail.xcenter.integration.WebServiceLogger;
//import dtv.data2.access.ObjectNotFoundException;

/**
 * This class is being called to make IBM Web Services instead of CW Serenade
 * @author Sumit Krishnan
 * @created Mar 15 2018
 */
 

public class IBMWebCommerceEndpoint {
	private static final String PROP_MIME_TYPE = "Content-Type";
	private static final String PROP_ACCEPT = "ACCEPT";
	private static final String PROP_USERNAME = "Username";
	private static final String PROP_PASSWORD = "Password";
	
	public IBMWebCommerceEndpoint() {
		
	}
	
	@ServiceActivator
	public Message callSerenade(Message argRequest) throws Exception {
		/*if(argRequest == null) {
			String reqMsg = "<Message source=\"XSTORE\" target=\"CUSTHISTIN\" type=\"CWCUSTHISTIN\"><CustomerHistoryRequest company=\"3\" direct_order_number=\"CRDEV00011\" direct_order_ship_to_nbr=\"228\" send_detail=\"Y\" /></Message>";
			argRequest = MessageBuilder.withPayload(reqMsg).build();
		}*/
		logRequest(argRequest);
		String apiString = (String) argRequest.getPayload();
		LOG.warn("IBMWebCommerceRestEndpoint::REST:: callSerenade: apiString is "+apiString);
		boolean isWebOrderReq = isWebOrderRequest(apiString);
		HttpsURLConnection conn = null;
		Message response = null;
		try{
			String urlStr = getConfigValue(
					"xcenter.IBMWebService.rest.getOrder.url",
					"https://chr-oms.qa.coc.ibmcloud.com/smcfs/restapi/executeFlow/RDSGetOrderDetailsForPOSSyncService");
			if(!isWebOrderReq) {
//				change urlString
				urlStr = getConfigValue("xcenter.IBMWebService.rest.returnOrder.url", "https://chr-oms.qa.coc.ibmcloud.com/smcfs/restapi/executeFlow/RDSCreateReturnForPOSSyncService");
			}
			LOG.warn("IBMWebCommerceRestEndpoint::REST:: callSerenade: IBMWebServiceURL "+ urlStr);
			URL url = new URL(urlStr);
			conn = (HttpsURLConnection) url.openConnection();
			conn.setHostnameVerifier(getHostNameVerifier());
			conn.setRequestMethod(getConfigValue("xcenter.IBMWebService.requestMethod", "POST"));
			LOG.warn("IBMWebCommerceRestEndpoint::REST:: callSerenade: IBMWebService Request Method "+ getConfigValue("xcenter.IBMWebService.requestMethod", "POST"));
			conn.setRequestProperty(PROP_MIME_TYPE, getConfigValue("xcenter.IBMWebService.MIMEType", "application/xml"));
			conn.setRequestProperty(PROP_ACCEPT, getConfigValue("xcenter.IBMWebService.MIMEType", "application/xml"));
			LOG.warn("IBMWebCommerceRestEndpoint::REST:: callSerenade: IBMWebService MIME Type "+ getConfigValue("xcenter.IBMWebService.MIMEType", "application/xml"));
			conn.setRequestProperty(PROP_USERNAME, getConfigValue("xcenter.IBMWebService.user", "crrestuser"));
			LOG.warn("IBMWebCommerceRestEndpoint::REST:: callSerenade: IBMWebServiceUser "+ getConfigValue("xcenter.IBMWebService.user", "crrestuser"));
			conn.setRequestProperty(PROP_PASSWORD, getConfigValue("xcenter.IBMWebService.password", "crrestpassword"));
			LOG.warn("IBMWebCommerceRestEndpoint::REST:: callSerenade: IBMWebServicePassword "+ getConfigValue("xcenter.IBMWebService.password", "crrestpassword"));
			String stripeChar = getConfigValue("xcenter.IBMWebService.WebOrderChar", "CRDEV04");
			LOG.warn("IBMWebCommerceRestEndpoint::REST:: callSerenade: stripeChar "+ stripeChar);
			
			conn.setDoOutput(true);
			
			DataOutputStream daos = null;
			try{
				daos = new DataOutputStream(conn.getOutputStream());
//			daos.write("<Message><CustomerHistoryRequest direct_order_number='CRDEV00011227' company='CR' /></Message>".getBytes());
				String apiIBMString = null;
				if(isWebOrderReq) {
					apiIBMString = convertCWReqToIBMReq(apiString);
				} else {
					apiIBMString = padWebOrderNo(apiString, stripeChar);
				}
				LOG.warn("IBMWebCommerceRestEndpoint::REST:: callSerenade: Sending: "+ apiIBMString);
				if(apiIBMString != null) {
					daos.write(apiIBMString.getBytes());
				}
				daos.flush();
			}finally{
				if(daos != null) {
					daos.close();
				}
			}
			
			int respCode = conn.getResponseCode();
			if(respCode != 200) {
				LOG.warn("Error occurred! response code received: "+respCode);
				throw new RuntimeException();
			}
			
			
			String responseIBMXML = getStringResponse(conn.getInputStream());
			LOG.warn("IBMWebCommerceRestEndpoint::REST:: callSerenade: respXML "+ responseIBMXML);
			String responseXML = null;
			if(isWebOrderReq) {
				responseXML = stripeWebOrderNo(responseIBMXML, stripeChar);
			} else {
				responseXML = responseIBMXML;
			}
			response = MessageBuilder.withPayload(responseXML).copyHeaders(argRequest.getHeaders()).build();
			
/*			
			if (isWebOrderRequest(apiString)){
				LOG.warn("IBMWebCommerceRestEndpoint::REST:: callSerenade: Calling Web Order Details");
				String apiIBMString = convertCWReqToIBMReq(apiString);
				//String shipToNum= getShipToNum(apiString);			
				String responseIBMXML = ws.rdsGetOrderDetailsForPOS(envString,apiIBMString);
				String responseCWXML=stripeWebOrderNo(responseIBMXML,stripeChar);
				response = MessageBuilder.withPayload(responseCWXML).copyHeaders(argRequest.getHeaders()).build();				
			}else {
				LOG.warn("IBMWebCommerceRestEndpoint::REST:: callSerenade: Calling Return Item Updates");
				String returnIBMapiString=padWebOrderNo(apiString,stripeChar);
				String returnIBMXMLRes = ws.rdsCreateReturnForPOS(envString,returnIBMapiString);
				response = MessageBuilder.withPayload(returnIBMXMLRes).copyHeaders(argRequest.getHeaders()).build();
			}		
			logResponse(response);
			return response;
*/			
		}catch(Exception ex) {
			ex.printStackTrace();
			LOG.warn("IBMWebCommerceRestEndpoint::REST:: callSerenade: Failed to contact IBM Web Service.");
			/*if (FailoverException.isFailover(ex)) {
				String msg = "IBMWebCommerceRestEndpoint::REST:: Failed to contact IBM Web Service.";
				if (LOG.isDebugEnabled())
					LOG.warn(msg, ex);
				else
					LOG.warn((new StringBuilder()).append(msg).append(" %s")
							.toString(), new Object[] { ex.toString() });
				throw new RuntimeException(msg, ex);
//				throw new ObjectNotFoundException(msg, ex);
			} else {*/
				throw ex;
//			}
		} finally {
			if(conn != null) {
				conn.disconnect();
			}
		}
		return response;
	}
	
	private String getStringResponse(InputStream inputStream) throws IOException {
		String retString = null;
		BufferedReader reader = null;
		StringBuilder builder = new StringBuilder();
		if(inputStream != null) {
			reader = new BufferedReader(new InputStreamReader(inputStream));
			String line = null;
			
			try {
				while((line = reader.readLine()) != null) {
					builder.append(line);
				}
			} catch (IOException e) {
				throw e;
			}finally {
				if(reader != null) {
					reader.close();
				}
			}
		} else {
			throw new RuntimeException("Null response received: "+inputStream);
		}
		
		if(builder != null) {
			retString = builder.toString();
		}
		return retString;
	}
	
	private HostnameVerifier getHostNameVerifier() {
		return new HostnameVerifier() {
			
			@Override
			public boolean verify(String arg0, SSLSession arg1) {
				return true;
			}
		};
	}
	
	
	protected void logRequest(Message argRequest) {
		WebServiceLogger
				.debug(this,
						"CWMessageIn",
						com.micros_retail.xcenter.integration.WebServiceLogger.SerivceDirection.REQUEST,
						(String) argRequest.getPayload());
	}
	
	protected void logResponse(Message argResponse) {
		WebServiceLogger
				.debug(this,
						"CWMessageIn",
						com.micros_retail.xcenter.integration.WebServiceLogger.SerivceDirection.RESPONSE,
						(String) argResponse.getPayload());
	}

	private static final Logger LOG = Logger.getLogger();
	
	private String getConfigValue(String propertyKey, String defaultValue) {
		String value = XcenterProperties.getProperty(propertyKey);
		return StringUtils.isEmpty(value) ? defaultValue : value;
	}
	
	private  boolean isWebOrderRequest(String xmlStr) {
		boolean isWebOrderRequest =false;
		Document doc = convertStringToDocument(xmlStr);
		XPath xpath = XPathFactory.newInstance().newXPath();
		NodeList nodes;
		try {
			LOG.warn("IBMWebCommerceRestEndpoint::REST:: isWebOrderRequest: xmlStr "+ xmlStr);
			nodes = (NodeList) xpath.evaluate("//CustomerHistoryRequest", doc,XPathConstants.NODESET);
			LOG.warn("IBMWebCommerceRestEndpoint::REST:: isWebOrderRequest: nodes "+ nodes);
			LOG.warn("IBMWebCommerceRestEndpoint::REST:: isWebOrderRequest: nodes length"+ nodes.getLength());
			if (nodes.getLength()==1){
				isWebOrderRequest=true;
			}

			return isWebOrderRequest;
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return isWebOrderRequest;
	}
	
	private Document convertStringToDocument(String xmlStr) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(xmlStr)));
			return doc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private  String convertCWReqToIBMReq(String xmlStr) {
		Document doc = convertStringToDocument(xmlStr);
		XPath xpath = XPathFactory.newInstance().newXPath();
		NodeList nodes;
		try {
			nodes = (NodeList) xpath.evaluate("//CustomerHistoryRequest", doc,XPathConstants.NODESET);
			
			for (int idx = 0; idx < nodes.getLength(); idx++) {
				
				Node CWWebOrder = nodes.item(idx).getAttributes().getNamedItem("direct_order_number");
				String CWWebOrderValue = CWWebOrder.getNodeValue();
				Node CWShipToNum = nodes.item(idx).getAttributes().getNamedItem("direct_order_ship_to_nbr");
				String CWShipToNumValue = CWShipToNum.getNodeValue();
				String newWebOrder = CWWebOrderValue.concat(StringUtils.leftPad(CWShipToNumValue, 3, "0")).toUpperCase();
				LOG.warn("IBMWebCommerceRestEndpoint::REST:: convertCWReqToIBMReq: webOrder: " +newWebOrder);
				//String newWebOrder = CWWebOrderValue.concat(CWShipToNumValue);
				CWWebOrder.setNodeValue(newWebOrder);				
			}
			String IBMReq = convertDocumentToString(doc);
			
			return IBMReq;
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return xmlStr;
	}
	
	private  String stripeWebOrderNo(String xmlStr, String stripeChar) {
		LOG.warn("IBMWebCommerceRestEndpoint::REST:: stripeWebOrderNo: xmlStr: " +xmlStr);
		Document doc = convertStringToDocument(xmlStr);
		XPath xpath = XPathFactory.newInstance().newXPath();
		NodeList nodes;
		try {
			nodes = (NodeList)xpath.evaluate("//Header",doc, XPathConstants.NODESET);
			
			for (int idx = 0; idx < nodes.getLength(); idx++) {            
	            Node IBMWebOrder = nodes.item(idx).getAttributes().getNamedItem("order_id");
	            String IBMWebOrderValue = IBMWebOrder.getNodeValue();
	            
	            String webOrder=IBMWebOrderValue.replace(stripeChar,"");
	            
	            String CWWebOrder = webOrder;
	            try{
	            	int CWWebOrderNum = Integer.parseInt(webOrder);
	            	CWWebOrder=String.valueOf(CWWebOrderNum);
	            }catch(NumberFormatException nfe) {}
	            IBMWebOrder.setNodeValue(CWWebOrder);
	            
	            //Change Date Format
	            Node orderDate = nodes.item(idx).getAttributes().getNamedItem("order_date");
	            String orderDateValue = orderDate.getNodeValue();
	            
	            SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				try {
					Date newDate = dateFormat.parse(orderDateValue);	
		            SimpleDateFormat newDateFormat= new SimpleDateFormat("MMddyyyy");
		            String newOrderDate=newDateFormat.format(newDate);
		            orderDate.setNodeValue(newOrderDate);
		            
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}				
			String IBMResponse = convertDocumentToString(doc);
			return IBMResponse;
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return xmlStr;
	}
	
	private String padWebOrderNo(String xmlStr, String padChar) {

		Document doc = convertStringToDocument(xmlStr);
		XPath xpath = XPathFactory.newInstance().newXPath();
		NodeList nodes;
		try {
			nodes = (NodeList)xpath.evaluate("//Return",doc, XPathConstants.NODESET);
			
			for (int idx = 0; idx < nodes.getLength(); idx++) {            
	            Node IBMWebOrder = nodes.item(idx).getAttributes().getNamedItem("ohd_order_nbr");
	            String IBMWebOrderValue = IBMWebOrder.getNodeValue();
	            String webOrderLenth = getConfigValue("xcenter.IBMWebService.rest.WebOrderLength", "8");
	            String CWWebOrder=padChar.concat(StringUtils.leftPad(IBMWebOrderValue, Integer.parseInt(webOrderLenth), "0"));
	            IBMWebOrder.setNodeValue(CWWebOrder);
	      
			}				
			String IBMResponse = convertDocumentToString(doc);
			
			return IBMResponse;
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return xmlStr;
	}
	
	private String convertDocumentToString(Document doc) {
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer;
		try {
			transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			String output = writer.getBuffer().toString();
			
			return output;
		} catch (TransformerException e) {
			e.printStackTrace();
		}

		return null;
	}

	
	public static void main(String[] args) {
		try {
			new IBMWebCommerceEndpoint().callSerenade(null);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
