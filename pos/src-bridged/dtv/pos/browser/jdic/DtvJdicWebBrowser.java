package dtv.pos.browser.jdic;

import java.net.URL;
import org.jdesktop.jdic.browser.WebBrowser;
import org.jdesktop.jdic.browser.WebBrowserListenerAdapter;
import org.jdesktop.jdic.browser.WebBrowserEvent;
import org.jdesktop.jdic.browser.WebBrowserListener;
import dtv.pos.framework.op.OpResponseHelper;
import dtv.ui.UIServices;
import dtv.event.eventor.DefaultEventor;
import dtv.event.EventDescriptor;

public class DtvJdicWebBrowser
  extends WebBrowser 
{
  private static final long serialVersionUID = 1L;
  private WebBrowserListener listener;
  
  public DtvJdicWebBrowser(){
	  super();
	  
	  listener =  new DtvJdicWebBrowserListener();
	  addWebBrowserListener(listener);
  }
  
  public boolean willOpenWindow()
  {
    return false;
  }
  
  public boolean willOpenWindow(URL arg0)
  {
    return false;
  }
  
  
  public class DtvJdicWebBrowserListener extends WebBrowserListenerAdapter{
	  
	  public void nativeDied(WebBrowserEvent event){

		  DefaultEventor eventor = new DefaultEventor(new EventDescriptor(this));
		  eventor.post("WEBBROWSER_NATIVE_DIE");
		}
	  
	}
  
}
